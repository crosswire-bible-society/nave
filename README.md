# Nave

Nave's Topics were originally produced by Orville J. Nave, A.M., D.D., LL.D. while serving as a Chaplin in the United States Army.  He referred to this work as "the result of fourteen years of delightful and untiring study of the Word of God."